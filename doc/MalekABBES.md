13 novembre 2021 : 
- Brainstorming sur le sujet de la page web.
- Choix du nombre de pages ainsi que le style, le thème général et le contenu de chaque page.
(5 pages: "Welcome" Page, "All Product" Page, "Product Detail" Page, "Cart" Page et "Account" Page)
14 novembre 2021 :
- collection des données( images, logo, ...)
- collection des couleurs qu'on va utiliser tout au long du projet.
- réalisation des maquettes.
- réalisation des prototypes.
15 novembre 2021: 
- Division des tâches entre les membres de l'équipe.
- Familiarisation avec le codage HTML + CSS ET Javascript.
17 novembre 2021: 
- Familiarisation avec le codage HTML + CSS ET Javascript.
20 novembre 2021: 
- Familiarisation avec le codage HTML + CSS ET Javascript.
27/28 novembre 2021: 
- Réalisation de l'interface "WelcomePage.HTML" et "Style1.CSS".
- Rédaction du fichier "Readme.md" de notre projet.
- Familiarisation avec les commandes git et bitbucket.
4/5 décembre 2021: 
- Faire un meeting avec tous les membres du projet.
- Révision et retouche des codes des pages Web.
- Uploading du Projet sur BitBucket.