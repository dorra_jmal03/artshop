13 novembre 2021 : 
- on a fait un Brainstorming sur le sujet de la page web.
- on a choisie le nombre des pages, le thème général, le style et le contenu de chaque page.
- on a decider de faire 5 pages (Welcome, All Product, Product Detail, Cart, Account)

14 novembre 2021 :
- on choisit et regrouper les images, logo, palettes qu'on va utiliser  
- collection des données( images, logo, ...)
- réalisation des maquette et du prototype.

15 novembre 2021:
- On a divisé les  tâches entre nous.

Entre 16 novembre et 3 décembre 2021:
Réalisation des codes html + css + js de l'interface account comme suit:
- page html : "account.html"
- page css : "style.css"
- ajout du code js à la page account.html

4/5 décembre 2021: 
- Réunion avec le groupe pour finaliser le projet 
- Révision et retouche des codes des pages Web.
- Uploading du Projet sur BitBucket.