13 novembre 2021 : 
- on a fait un Brainstorming sur le sujet de la page web.
- on a choisie le nombre des pages, le thème général, le style et le contenu de chaque page.
- on a decider de faire 5 pages (Welcome, All Product, Product Detail, Cart, Account)
14 novembre 2021 :
- on choisit et regrouper les images, logo, palettes qu'on va utiliser  
- collection des données( images, logo, ...)
- réalisation des maquette et du prototype.
21 novembre 2021: j'ai realisé les codes HTML+CSS de la page "All Product".
- page html : products.html
- page css : allproductstyle.css
26 novembre 2021:  j'ai realisé les codes HTML+CSS de la page "cart".
- page html : cart.html
- page css : cartstyle.css
27 novembre 2021: addition de java script
4/5 décembre 2021: 
- Révision et retouche des codes des pages Web.
- Uploading du Projet sur BitBucket.