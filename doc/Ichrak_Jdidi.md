13 novembre 2021 : 
- Brainstorming sur le sujet de la page web.
- Choix du nombre de pages ainsi que le style, le thème général et le contenu de chaque page.
(5 pages: "WelcomePage", "AllProductPage", "ProductDetailPage", "CartPage" et "AccountPage" )
14 novembre 2021 :
- Collection des données( images, logo, ...)
- Collection des couleurs qu'on va utiliser tout au long du projet.
- Réalisation des maquettes.
- Réalisation du prototype.
20/21 novembre 2021: Réalisation des codes HTML+CSS de la page "Product Detail".
- Réalisation de la page HTML.
- Styling de la page "Product Detail" à l'aide de CSS.
4/5 décembre 2021: 
- Révision et retouche des codes des pages Web .
- Uploading du Projet sur BitBucket.