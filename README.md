# Art Shop 
***
Our project "Art Shop" is an e-commerce website which allows you to buy or sell artistic paintings. 
***
A simulation of client side e-commerce website with feature as add to cart, proceed for checkout and payment options. It simultes a shopping cart within a website.
****
Follow the following steps to view the project:
1.Download the folder <ArtShop> using clone or download option on the right of the repository. OR Create a folder in your laptop preferably on Desktop and then download the files inside the folder.
2.Once you are done with downloads please note that the names of files and images as well as the image folder must be same as the uploaded files in the repository since they have been used inside the codes with their paths.
****
Now you are all set to view the project.
3.open "WelcomePage.html"file with your browser(any).
4.For further simulation you will we instructed in the web page such as add to cart will take to "your cart" and then you can choose the more options on cart page.
****
I will further attach the screenshots of the webpages in screenshots folder.

Here is a similar but cleaner version of this project E-COMMERCE-STORE ==> https://github.com/vinita2000/E-Commerce-Website